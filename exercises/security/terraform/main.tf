// Для удобства всё находится в одном файле
// При необходимости разделяется на отдельные .tf файлы

// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs
terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

// Токен DO и путь к приватному ключу, будут передаваться через CLI
variable "do_token" {}

provider "digitalocean" {
  // Использование переменной (токен доступа к DO)
  // https://www.terraform.io/docs/language/values/variables.html
  token = var.do_token
}

// Ключ можно либо получить созданный, либо создать новый
// resource "digitalocean_ssh_key" "default" {
//   name       = "Terraform Homework"
//   public_key = file("~/.ssh/id_rsa.pub")
// }
// Используется data source - ресурс не создаётся. Terraform запрашивает информацию о ресурсе
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/data-sources/droplet
data "digitalocean_ssh_key" "ssh" {
  // Имя под которым ключ сохранён в DO
  // https://cloud.digitalocean.com/account/security
  name = "tabomors-ZenBook-14-UX410UFR"
}

output "webservers" {
  value = digitalocean_droplet.web
}

output "bastion" {
  value = digitalocean_droplet.bastion
}

// Создаём виртуальную сеть
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/vpc
resource "digitalocean_vpc" "vpc" {
  name   = "security-homework-vpc"
  region = "ams3"
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "lb" {
  name     = "security-homework-load-balancer"
  region   = "ams3"
  vpc_uuid = digitalocean_vpc.vpc.id

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port     = 80
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port     = 5000
    target_protocol = "http"
  }

  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет жив и принимает запросы
  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.web.*.id
}

// Создание домена
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/domain
resource "digitalocean_domain" "domain" {
  name       = "security-homework.idont.cyou"
  ip_address = digitalocean_loadbalancer.lb.ip
}

// Создаём дроплет
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet
resource "digitalocean_droplet" "web" {
  count    = 2
  image    = "docker-20-04"
  name     = "web-security-homework-web-${count.index + 1}"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.vpc.id

  // Добавление приватного ключа на создаваемый сервер
  // Обращение к datasource выполняется через data.
  ssh_keys = [
    data.digitalocean_ssh_key.ssh.id
  ]
}

// Создаём файрволл
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/firewall
resource "digitalocean_firewall" "web" {
  name = "security-homework-lb-and-ssh-only"

  droplet_ids = digitalocean_droplet.web.*.id

  inbound_rule {
    protocol           = "tcp"
    port_range         = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  inbound_rule {
    protocol                  = "tcp"
    port_range                = "5000"
    source_load_balancer_uids = [digitalocean_loadbalancer.lb.id]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_droplet" "bastion" {
  image  = "ubuntu-20-04-x64"
  name   = "security-bastion"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  vpc_uuid = digitalocean_vpc.vpc.id

  ssh_keys = [
    data.digitalocean_ssh_key.ssh.id
  ]
}

resource "digitalocean_firewall" "bastion" {
  name = "security-homework-ssh-only"

  droplet_ids = digitalocean_droplet.web.*.id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol   = "tcp"
    port_range = "1-65535"
  }

  outbound_rule {
    protocol   = "udp"
    port_range = "1-65535"
  }

  outbound_rule {
    protocol   = "icmp"
    port_range = "1-65535"
  }
}
