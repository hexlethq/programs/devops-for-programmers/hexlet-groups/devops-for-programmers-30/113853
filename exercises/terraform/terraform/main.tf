terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Set the variable value in *.tfvars file
# or using -var="do_token=..." CLI option
variable "do_token" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "default_key" {
  // Имя под которым ключ сохранён в DO
  // https://cloud.digitalocean.com/account/security
  name = "tabomors-ZenBook-14-UX410UFR"
}


# Create a web server
resource "digitalocean_droplet" "web1" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-01"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [data.digitalocean_ssh_key.default_key.id]
}

# Create a web server
resource "digitalocean_droplet" "web2" {
  image  = "docker-20-04"
  name   = "web-terraform-homework-02"
  region = "ams3"
  size   = "s-1vcpu-1gb"

  ssh_keys = [data.digitalocean_ssh_key.default_key.id]
}

// Создание балансировщика нагрузки
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/loadbalancer
resource "digitalocean_loadbalancer" "lb" {
  name   = "terraform-homework-lb"
  region = "ams3"

  forwarding_rule {
    // Порт по которому балансировщик принимает запросы
    entry_port     = 80
    entry_protocol = "http"

    // Порт по которому балансировщик передает запросы (на другие сервера)
    target_port     = 5000
    target_protocol = "http"
  }

  // Порт, протокол, путь по которому балансировщик проверяет, что дроплет живой и принимает запросы
  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [
    digitalocean_droplet.web1.id,
    digitalocean_droplet.web2.id
  ]
}

// Создание домена
// https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/domain
resource "digitalocean_domain" "terraform-homework_domain" {
  name       = "terraform-homework.idont.cyou"
  ip_address = digitalocean_loadbalancer.lb.ip
}

// Outputs похожи на возвращаемые значения. Они позволяют сгруппировать информацию или распечатать то, что нам необходимо
// https://www.terraform.io/docs/language/values/outputs.html
output "droplets_ips" {
  // Обращение к ресурсу. Каждый ресурс имеет атрибуты, ккоторым можно получить доступ
  value = [
    digitalocean_droplet.web1.ipv4_address,
    digitalocean_droplet.web2.ipv4_address
  ]
}

output "lb_ip" {
  value = digitalocean_loadbalancer.lb.ip
}